#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple Bot to reply to Telegram messages.

This program is dedicated to the public domain under the CC0 license.

This Bot uses the Updater class to handle the bot.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from telegram.ext import *
from telegram import *
import logging,commands,os,random,string,json,urllib,urllib2,os,time,subprocess,sys
reload(sys)
sys.setdefaultencoding('utf-8')
# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
					level=logging.INFO)

logger = logging.getLogger(__name__)

langlist = ["az","sq","am","en","ar","hy","af","eu","ba","be","bn",
"my","bg","bs","cy","hu","vi","ht","gl","nl","mrj","el","ka","gu","da",
"he","yi","id","ga","it","is","es","kk","kn","ca","ky","zh","ko","xh",
"km","lo","la","lv","lt","lb","mg","ms","ml","mt","mk","mi","mr","mhr",
"mn","de","ne","no","pa","pap","fa","pl","pt","ro","ru","ceb","sr","si",
"sk","sl","sw","su","tg","th","tl","ta","tt","te","tr","udm","uz","uk",
"ur","fi","fr","hi","hr","cs","sv","gd","et","eo","jv","ja"]

transkey = "trnsl.1.1.20180324T015610Z.c2d40b55e2b32c92.31cc68c2b30383d597c0759ae6a0eaae86117512"

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
	"""Send a message when the command /start is issued."""
	update.message.reply_text('Hi! '+update.message.from_user.first_name +" ("+str(update.message.chat_id)+")")

def help(bot, update):
	"""Send a message when the command /start is issued."""
	update.message.reply_text(subprocess.check_output("cat README.md", shell=True))

def myid(bot, update):
	"""Send a message when the command /start is issued."""
	update.message.reply_text((str(update.message)))

def block_shell(txt):
	txt=txt.replace(";","")
	txt=txt.replace("$","")
	txt=txt.replace("&","")
	txt=txt.replace("|","")
	txt=txt.replace("{","")
	txt=txt.replace("}","")
	return txt

def echo(bot, update):
	"""Echo the user message."""
	txt=""+update.message.text
	print (update.message.text)
	txt=txt.replace("@paledega_bot","")
	if("/uptime" == txt.split(" ")[0]):
		data=subprocess.check_output("uptime", shell=True)
		data=data+"\n"+subprocess.check_output("uname -a", shell=True)
		update.message.reply_text(data)
		print data
	elif("/echo" == txt.split(" ")[0]):
		data=txt.replace("/echo","")
		update.message.reply_text(data)
	elif("/youtube" == txt.split(" ")[0]):
		if ("result" in txt):
			update.message.reply_text("Bana arama sonucu değil video linki yolla...")
		elif("youtu.be/" in txt or "youtube.com/" in txt or "youtube.com/" in txt):
			yt_audio(txt.replace("/youtube ",""),update)
		else:
			update.message.reply_text("Geçersiz link gönderdin...")
	elif("/github" == txt.split(" ")[0]):
		if (len(txt.split(" ")) > 1 and "https://github.com/" in txt):
			txt=txt.replace("/github ","").replace("https://github.com/","")
			update.message.reply_text("İndiriyorum...")
			if("/github@" in txt.split("")[0]):
				branch=txt.split("")[0].split("@")[1]
			else:
				branch="master"
			os.system("wget -c https://github.com/"+txt.split("/")[0]+"/"+txt.split("/")[1]+"/archive/"+branch+".zip -O "+txt.split("/")[1]+"-master.zip")
			update.message.reply_text("Sana Yolluyorum..")
			send_file(txt.split("/")[1]+"-master.zip",update)
			os.system("rm -f "+txt.split("/")[1]+"-master.zip")
		else:
			update.message.reply_text("Hatalı adres. Bunu indiremem...")
	elif("/date" == txt.split(" ")[0]):
		data=subprocess.check_output("date", shell=True)
		print data
		update.message.reply_text(data)
	elif("/calc" == txt.split(" ")[0]):
		numeric="1234567890"
		character="*/\\+-)(fi<>=!%"
		txt2=""
		txt=txt.replace("/calc ","")
		for a in txt:
			if (a in numeric):
				txt2=txt2+a
			elif(a in character):
				txt2=txt2+a
		txt2=txt2.replace("f","float")
		txt2=txt2.replace("i","int")
		txt2=txt2.replace("**","*")
		print txt2
		data=subprocess.check_output("echo \"print "+txt2+"\" | python 2> /dev/null &", shell=True)
		print data
		if len(data)>=1:
			update.message.reply_text(txt2+":\n"+data)
		else:
			update.message.reply_text("Math Error")
	elif("/weather" == txt.split(" ")[0]):
		city=txt.replace("/weather ","")
		city=block_shell(city)
		data=subprocess.check_output("curl \"http://wttr.in/"+city+"?qT0\"", shell=True)
		update.message.reply_text(data)
		print city
	elif("/whois" == txt.split(" ")[0]):
		site=txt.replace("/whois ","")
		site=block_shell(site)
		data=subprocess.check_output("whois "+site+";exit 0",stderr=subprocess.STDOUT,shell=True)
		update.message.reply_text(data)
		print site
	elif("/trans-en" in txt.split(" ")[0]):
		trans("en",txt,update)
	elif("/trans-es" in txt.split(" ")[0]):
		trans("en",txt,update)
	elif("/trans-de" in txt.split(" ")[0]):
		trans("de",txt,update)
	elif("/trans-ar" in txt.split(" ")[0]):
		trans("ar",txt,update)
	elif("/trans-ja" in txt.split(" ")[0]):
		trans("ja",txt,update)
	elif("/trans-it" in txt.split(" ")[0]):
		trans("it",txt,update)
	elif("/trans-fr" in txt.split(" ")[0]):
		trans("fr",txt,update)
	elif("/trans-ch" in txt.split(" ")[0]):
		trans("zh-CN",txt,update)
	elif("/trans" in txt.split(" ")[0]):
		trans("tr",txt,update)
	elif("/tts-en" in txt.split(" ")[0]):
		tts("en",txt,update)
	elif("/tts-es" in txt.split(" ")[0]):
		tts("en",txt,update)
	elif("/tts-de" in txt.split(" ")[0]):
		tts("de",txt,update)
	elif("/tts-ar" in txt.split(" ")[0]):
		tts("ar",txt,update)
	elif("/tts-ja" in txt.split(" ")[0]):
		tts("ja",txt,update)
	elif("/tts-it" in txt.split(" ")[0]):
		tts("it",txt,update)
	elif("/tts-fr" in txt.split(" ")[0]):
		tts("fr",txt,update)
	elif("/tts-ch" in txt.split(" ")[0]):
		tts("zh-CN",txt,update)
	elif("/tts" in txt.split(" ")[0]):
		tts("tr",txt,update)
	elif("/trans" in txt.split(" ")[0]):
		if (txt.split(" ")[0].split("-")[1] in langlist):
			trans(txt.split(" ")[0].split("-")[1],txt,update)
		else:
			trans("tr",txt,update)
	elif("/man" == txt.split(" ")[0]):
		txt=txt.replace("/man ","")
		txt=txt.replace("*","")
		txt=txt.replace(" ","")
		txt=txt.replace("-","")
		txt=txt.replace("_","")
		txt=txt.replace("/","")
		txt=txt.replace("//","")
		os.system("man \""+block_shell(txt)+"\" > "+"man_"+block_shell(txt)+"_"+str(update.message.chat_id).replace("-","_")+".txt | cat 2> "+"man_"+block_shell(txt)+"_"+str(update.message.chat_id).replace("-","_")+".txt")
		send_file("man_"+block_shell(txt)+"_"+str(update.message.chat_id).replace("-","_")+".txt",update)
		os.system("rm -f "+"man_"+block_shell(txt)+"_"+str(update.message.chat_id).replace("-","_")+".txt")
	elif("/sed" == txt.split(" ")[0]):
		txt=txt.replace("/sed ","")
		command=txt.split("\n")[0]
		command=command.replace('"','\\"')
		i=1
		txt=txt.replace("\"","\\\"")
		os.system("echo > \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
		while i != len(txt.split("\n")):
			if txt.split("\n")[i].encode('utf-8') != "":
				os.system("echo \""+txt.split("\n")[i].encode('utf-8')+"\" >> "+"raw"+str(update.message.chat_id).replace("-","_"))
			else: 
				os.system("echo \"\\\n\" >> \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
			i=i+1
		try:
			seddata=""
			for line in command.split(" | "):
				seddata=seddata+" | sed \""+block_shell(line)+"\""
				print line
			data=subprocess.check_output("cat \"raw"+str(update.message.chat_id).replace("-","_")+"\""+seddata, shell=True)
			os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
			if(len(data)>0):
				update.message.reply_text(data)
			else:
				update.message.reply_text("Syntax Error")
				os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
		except:
			update.message.reply_text("Invalid Syntax")
			os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
	elif("/grep" == txt.split(" ")[0]):
		txt=txt.replace("/grep ","")
		command=txt.split("\n")[0]
		command=command.replace('"','\\"')
		i=1
		txt=txt.replace("\"","\\\"")
		os.system("echo > \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
		while i != len(txt.split("\n")):
			if txt.split("\n")[i].encode('utf-8') != "":
				os.system("echo \""+txt.split("\n")[i].encode('utf-8')+"\" >> "+"raw"+str(update.message.chat_id).replace("-","_"))
			else: 
				os.system("echo \"\\\n\" >> \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
			i=i+1
		try:
			seddata=""
			for line in command.split(" | "):
				seddata=seddata+" | grep \""+block_shell(line)+"\""
				print line
			data=subprocess.check_output("cat \"raw"+str(update.message.chat_id).replace("-","_")+"\""+seddata, shell=True)
			os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
			if(len(data)>0):
				update.message.reply_text(data)
			else:
				update.message.reply_text("Syntax Error")
				os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
		except:
			update.message.reply_text("Invalid Syntax")
			os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
	elif("/termbin" == txt.split(" ")[0] and len(txt.split(" ")) >1):
		txt=txt.replace("/termbin ","")
		txt=block_shell(txt)
		i=0
		os.system("echo > \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
		while i != len(txt.split("\n")):
			if txt.split("\n")[i].encode('utf-8') != "":
				os.system("echo \""+txt.split("\n")[i].encode('utf-8')+"\" >> "+"raw"+str(update.message.chat_id).replace("-","_"))
			else: 
				os.system("echo \"\\\n\" >> \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
			i=i+1
		try:
			data=subprocess.check_output("cat \"raw"+str(update.message.chat_id).replace("-","_")+"\" | nc termbin.com 9999", shell=True)
			os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
			if(len(data)>0):
				update.message.reply_text(data)
			else:
				update.message.reply_text("Syntax Error")
				os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
		except:
			update.message.reply_text("Invalid Syntax")
			os.system("rm -f \""+"raw"+str(update.message.chat_id).replace("-","_")+"\"")
	else:
		update.message.reply_text("Bunu anlamadım ")

def inline(bot, update):
	query = update.inline_query.query
	tx = query.split(" ")
	sres = []
	if 1==1:
		temp=query[len(tx[0])+1:]
		cmd=tx[0][1:]
		if (1==1):
			desc=u"Çevir: "+temp
			if len(tx) < 2:
				desc="Bu özelliği kullanmak için bir metin yazmalısınız!"
				temp=desc
			else:
				if tx[0] in langlist:
					lang =tx[0]
				else:
					lang = "tr"
				print(lang)
				temp = urllib.quote(temp.encode("utf8"))
				temp = ''.join(temp).replace("?","%3F")
				temp = "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+transkey+"&lang="+lang+"&text="+temp
				print temp
				temp = urllib2.urlopen(temp)
				temp = json.loads(temp.read()).get("text")[0]
				desc = desc + "\n" + u"Önizleme: " + temp
			sres.append(InlineQueryResultArticle(
					id = ''.join(random.choice(string.ascii_lowercase + string.digits))[:8],
					title = bot.first_name,
					#thumb_url = "http://python-telegram-bot.readthedocs.io/en/latest/_static/ptb-logo-orange.png",
					description=desc,
					input_message_content = InputTextMessageContent(temp)
				)
			)
		else:
			return
		update.inline_query.answer(sres)
	else:
		return
		
def error(bot, update, error):
	"""Log Errors caused by Updates."""
	logger.warning('Update "%s" caused error "%s"', update, error)

def send_file(directory,update):
	chatid=str(update.message.chat_id)
	curl_exec='curl -s -X POST "https://api.telegram.org/bot396146405:AAGnSqRBiMp2ah_2S06gA31EALw414Cgw64/sendDocument" -F chat_id="'+chatid+'" -F document="@'+directory+'"'
	if ("413 Request Entity Too Large" in subprocess.check_output(curl_exec,shell=True)):
		os.system("rm -f "+directory)
		update.message.reply_text("Dosya fazla büyük yollayamıyorum...")
def send_photo(directory,update):
	chatid=str(update.message.chat_id)
	curl_exec='curl -s -X POST "https://api.telegram.org/bot396146405:AAGnSqRBiMp2ah_2S06gA31EALw414Cgw64/sendPhoto" -F chat_id="'+chatid+'" -F photo="@'+directory+'"'
	os.system(curl_exec)

def send_audio(directory,update):
	chatid=str(update.message.chat_id)
	curl_exec='curl -s -X POST "https://api.telegram.org/bot396146405:AAGnSqRBiMp2ah_2S06gA31EALw414Cgw64/sendAudio" -F chat_id="'+chatid+'" -F audio="@'+directory+'"'
	os.system(curl_exec)

def yt_audio(link,update):
	chatid=str(update.message.chat_id)
	update.message.reply_text("İndirip dönüştürüyorum...")
	directory="music.ogg"
	if ("youtu.be" in link):
		directory=link.split("/")[len(link.split("/"))-1]
		link="https://www.youtube.com/watch?v="+directory
		directory=directory+".ogg"
	else:
		directory=link.split("=")[1]+".ogg"
	link=link.replace("\n","")
	os.system("./youtube-dl -x --no-playlist -v -f best --audio-format vorbis --audio-quality 9 "+link+" -o "+directory)
	print directory
	update.message.reply_text("Sana Yolluyorum")
	send_file(directory,update)
	os.system("rm -rf *.ogg")

def download(bot, update):
	file_id = update.message.document.file_id
	newFile = bot.getFile(file_id)
	bot.sendMessage(chat_id=update.message.chat_id, text="Dosyayı aldım...")
	fname=update.message.document.file_name.lower()
	fuzanti="."+fname.split(".")[len(fname.split("."))-1]
	if ((".doc" in fuzanti) or (".ppt" in fuzanti) or (".pps" in fuzanti) or (".od" in fuzanti) or (".xls" in fuzanti)):
		newFile.download(update.message.document.file_id)
		bot.sendMessage(chat_id=update.message.chat_id, text="PDF yapıyorum...")
		os.system("libreoffice --convert-to pdf "+update.message.document.file_id)
		bot.sendMessage(chat_id=update.message.chat_id, text="Sana yolluyorum...")
		send_file(update.message.document.file_id+".pdf",update)
		os.system("rm -rf  "+update.message.document.file_id+".pdf")
		os.system("rm -rf  "+update.message.document.file_id)
	elif ((".c" in fuzanti) or (".h" in fuzanti) or (".java" in fuzanti)or (".cpp" in fuzanti)):
		newFile.download(update.message.document.file_id)
		bot.sendMessage(chat_id=update.message.chat_id, text="Girintileme hatalarını gideriyorum...")
		os.system("python girintile.py "+update.message.document.file_id+" > "+update.message.document.file_id+fuzanti)
		bot.sendMessage(chat_id=update.message.chat_id, text="Sana yolluyorum...")
		send_file(update.message.document.file_id+fuzanti,update)
		os.system("rm -rf  "+update.message.document.file_id+fuzanti)
		os.system("rm -rf  "+update.message.document.file_id)
	
	else:
		bot.sendMessage(chat_id=update.message.chat_id, text="Bu dosyayla ne yapacağımı anlamadığım için siliyorum")

def tts(lang,txt,update):
		txt=txt.replace(txt.split(" ")[0],"")
		os.system("mplayer \"http://translate.googleapis.com/translate_tts?ie=UTF-8&client=gtx&tl="+lang+"&q="+txt+"\" -ao pcm:fast:waveheader:file=tts.wav")
		send_audio("tts.wav",update)
		os.system("rm -f tts.wav")
 	
	
def trans(lang,txt,update):
		txt=txt.replace(txt.split(" ")[0],"")
		txt=block_shell(txt)
		txt=txt.replace(". ",".\n")
		data=""
		i=0
		sat=""
		for line in txt.split("\n"):
			if(len(line)>1):
				data=data+subprocess.check_output("trans -e yandex -b :"+lang+" \""+line+"\"", shell=True).replace("\n","")
		update.message.reply_text(data)
 
def main():
	"""Start the bot."""
	# Create the EventHandler and pass it your bot's token.
	updater = Updater("396146405:AAGnSqRBiMp2ah_2S06gA31EALw414Cgw64")

	# Get the dispatcher to register handlers
	dp = updater.dispatcher

	# on different commands - answer in Telegram
	dp.add_handler(CommandHandler("start", start))
	
	dp.add_handler(CommandHandler("me", myid))
	
	dp.add_handler(CommandHandler("help", help))
	dp.add_handler(InlineQueryHandler(inline))

	# on noncommand i.e message - echo the message on Telegram
	dp.add_handler(MessageHandler(Filters.command, echo))

	document_handler = MessageHandler(Filters.document, download)
	dp.add_handler(document_handler)

	# log all errors
	dp.add_error_handler(error)

	# Start the Bot
	updater.start_polling()

	# Run the bot until you press Ctrl-C or the process receives SIGINT,
	# SIGTERM or SIGABRT. This should be used most of the time, since
	# start_polling() is non-blocking and will stop the bot gracefully.
	updater.idle()


if __name__ == '__main__':
	main()



